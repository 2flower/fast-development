import React from 'react'
import Image from 'next/image'

interface iFellowCard {
    image: string,
    cardName: string, 
    quote: string,
    text: string,
}

const FellowCard = ({image, cardName, quote, text}: iFellowCard) => {
  return (
    <div className='fellow-card pb-5'>
      <div className='image-row'>
        <Image 
            className='mw-100 mh-100'
            src={image} 
            alt={image}
            width={0}
            height={0}
            sizes="100vw"
            style={{ width: '100%', height: 'auto' }} 
            priority={false}
        />
      </div>
      <div className='text-row d-flex flex-column'>
        <h4 className={`text-black fw-light py-3`}>Raphaëlle</h4>
        <div className='d-flex direction-row'>
          <span>
            <Image 
                className='mw-100 mh-100 d-inline-flex'
                src={`/icons/pseudo.png`} 
                alt="fellow_3.jpg"
                width={23}
                height={23}
            />
          </span>
          <blockquote className='fw-light text-blue d-inline-flex ps-2'>{quote}</blockquote>
        </div>
        <p className='fw-light text-black'>{text}</p>
      </div>
    </div>
  )
}

export default FellowCard;
