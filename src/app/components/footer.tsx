import React from 'react';
import Link from 'next/link';
import Image from 'next/image';

export default function Footer() {
  return (
    <footer className="py-3 mt-5 bg-black text-white text-center">
        <div className='logo pt-3 d-flex justify-content-center'>
        <Image
            src={`footer_logo.svg`}
            alt="Foot Logo"
            width={`177`}
            height={`55`}
        />
        </div>
        <div className='contact-info my-3 fw-light direction-column'>
        <div className='pages my-3 py-3 d-flex justify-content-between border-top border-bottom'>
            <Link href="/fellowships" className=''>Fellowships</Link>
            <Link href="/about" className=''>About</Link>
            <Link href="/join-us" className=''>Join Us</Link>
            <Link href="/blog" className=''>Blog</Link>
            <Link href="/impact-academy-fund" className='no-wrap'>Impact Academy Fund</Link>
        </div>
        <div className='contact'>
            <div className='email d-flex flex-column pe-2 py-3'>
            <p className='d-flex m-0'>Email</p>
            <a className='d-flex' href="mailto:info@impactacademy.org">info@impactacademy.org</a>
            </div>
            <div>
            <p className='address text-start px-2 py-3'>71-75 Shelton Street, Covent Garden, London, United Kingdom, WC2H 9JQ</p>
            </div>
            <div className='social ps-2 py-3 d-inline-flex justify-content-end align-items-end'>
            <a href="https://www.twitter.com/" target="_blank">
                <Image
                className='me-3'
                src={`/icons/twitter.svg`}
                alt="twitter / X logo"
                width={`30`}
                height={`30`}
                priority={false}
                />
            </a>
            <a href="https://www.linkedin.com/" target="_blank">
            <Image
                className='linkedin'
                src={`/icons/linkedin.svg`}
                alt="LinkedIn Logo"
                width={`29`}
                height={`29`}
                priority={false}
                />
            </a>
            </div>
        </div>
        </div>
        <div className='policies fw-light my-3 pt-3 d-flex flex-column justify-content-end align-items-end flex-nowrap'>
        <p className='m-0'>Cookie Policy | Privacy Policy</p>
        <p className='m-0'>Designed by And--Now</p>
        </div>
    </footer>
  )
}
