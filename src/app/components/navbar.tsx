"use client"
import React, { useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';

export default function Navbar() {
    const [show, setShow] = useState(false);

  return (
    <nav className="navbar navbar-expand-lg px-sm-5">
        <div className="container-fluid">
            <Link className="navbar-brand me-0" href="/">
                <Image
                src="/logo-image.svg"
                alt="logo image"
                width={55}
                height={55}
                />
            </Link>
            <button 
                className="navbar-toggler" 
                type="button" 
                data-bs-toggle="collapse" 
                aria-controls="navbarSupportedContent" 
                aria-expanded="false" 
                aria-label="Toggle navigation"
                onClick={() => setShow(prevShow => !prevShow)}
            >
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className={`collapse navbar-collapse ${(show) ? "show" : ""}`} >
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className={`nav-item`}>
                    <Link 
                        className={`btn-rounded btn-white btn-nav fw-lighter nav-link active`} 
                        aria-current="page" 
                        href="/fellowships"
                        onClick={() => setShow(false)}
                    >
                            Fellowships
                    </Link>
                </li>
                <li className={`nav-item`}>
                    <Link 
                        className={`btn-rounded btn-white btn-nav fw-lighter nav-link`} 
                        aria-current="page" 
                        href="/about"
                        onClick={() => setShow(false)}
                    >
                            About Us
                    </Link>
                </li>
                <li className={`nav-item`}>
                    <Link
                        className={`btn-rounded btn-white btn-nav fw-lighter nav-link`} 
                        aria-current="page" 
                        href="/join-us"
                        onClick={() => setShow(false)}
                    >
                            Join Us
                    </Link>
                </li>
                <li className={`nav-item`}>
                    <Link 
                        className={`btn-rounded btn-white btn-nav fw-lighter nav-link`} 
                        aria-current="page" 
                        href="/blog"
                        onClick={() => setShow(false)}
                    >
                        Blog
                    </Link>
                </li>
                <li className={`nav-item`}>
                    <Link 
                        className={`btn-rounded btn-blue btn-nav fw-lighter nav-link`} 
                        aria-current="page" 
                        href="/impact-academy-fund"
                        onClick={() => setShow(false)}
                    >
                        Impact Academy Fund
                    </Link>
                </li>
                </ul>
            </div>
        </div>
    </nav>
  )
}
