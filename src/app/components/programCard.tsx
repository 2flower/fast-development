import React from 'react'
import Image from 'next/image'

interface iProgramCard {
    image: string,
    icon: string,
    iconSize: number,
    cardTitle: string, 
    programTime: string, 
    text: string,
    btnLabel: string
}

const ProgramCard = ({image, icon, iconSize, cardTitle, programTime, text, btnLabel}: iProgramCard) => {
  return (
    <div className={`program-card py-3`}>
        <div className={`image-row`}>
            <Image 
                className='mw-100 mh-50'
                src={image} 
                alt={image} 
                width={0}
                height={0}
                sizes="100vw"
                style={{ width: '200%', height: 'auto' }} 
                priority={false}
            />
        </div>
        <div className='text-row'>
            <div className='py-2'>
                <Image 
                    className='mw-100 mh-50'
                    src={icon} 
                    alt="program 1 image"
                    width={iconSize}
                    height={iconSize}
                />
                <h3 className="text-black fw-light">{cardTitle}</h3>
                <span className='program-time text-black fw-light'>{programTime}</span>
            </div>
            <p className="text-black">{text}</p>
            <div>
                <button className='btn-rounded btn-large btn-blue mt-2'>{btnLabel}</button>
                <button className='btn-rounded btn-large btn-beige mt-2'>More Information</button>
            </div>
        </div>
    </div>
  )
}

export default ProgramCard;
