import Image from 'next/image'
import styles from './page.module.css'
import stylesSCSS from './page.module.scss'
import ProgramCard from './components/programCard'
import FellowCard from './components/fellowCard'

export default function Home() {
  return (
    <main className={`${stylesSCSS.bgBaige} `}>
      <div className='container'>
        <section>
          <div className='row'>
            <div className='col-md-6 col-sm-12 d-flex flex-column justify-content-center pb-5'>
              <h1 className='text-black'>Empowering Future Change-Makers</h1>
              <button className={`btn-rounded btn-blue btn-large w-fit-content fw-lighter`}>
                <span>Apply to our fellowship</span> 
                <span className='ps-2'>
                  <Image
                    src="/arrow-right.png"
                    alt="arrow pointing right"
                    width={16}
                    height={16}
                  />
                </span> 
              </button>
            </div>
            <div className='col-md-6 col-sm-12'>
                  <div className='image-right'>
                    <Image
                      src="/home-img-1.png"
                      alt="image"
                      width={0}
                      height={0}
                      sizes="100vw"
                      style={{ width: '100%', height: 'auto' }} 
                      priority
                    />
                  </div>
            </div>
          </div>
        </section>
        <section className='pt-5 d-block'>
          <div className='row'>
            <div className='col'>
              <h2 className={`btn-rounded text-black bg-white text-center fw-light py-3 my-5`}>Current Programs</h2>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-6 col-sm-12'>
                <ProgramCard 
                  image="/programs/program-box-2.png"
                  icon="/icons/future_academy_icon.svg"
                  iconSize={35}
                  cardTitle="AI Governance Fellowship" 
                  programTime="5 months" 
                  text="A program developed to provide a solid foundation for young and aspiring policy professionals. Set yourself up to shape the future of artificial intelligence through effective regulation and governance."
                  btnLabel="Stay Informed"
                />
            </div>
            <div className='col-md-6 col-sm-12'>
              <ProgramCard 
                image="/programs/program-box-1.png"
                icon="/icons/AI_governance_icon.svg"
                iconSize={25}
                cardTitle="AI Governance Fellowship" 
                programTime="2 months" 
                text="A program developed to provide a solid foundation for young and aspiring policy professionals. Set yourself up to shape the future of artificial intelligence through effective regulation and governance."
                btnLabel="Stay Informed"
              />
            </div>
          </div>
        </section>
        <section>
          <div className='row'>
            <div className='col'>
              <h2 className={`btn-rounded text-black bg-white text-center fw-light py-3 my-5`}>Our Fellows</h2>
            </div>
          </div>
          <div className='row'>
            <div className='col-md-4 col-sm-12'>
              <FellowCard
                  image='/fellows/div.common-fellow-box_img.png'
                  cardName="Abdulrahman"
                  quote="Even the brightest light on a full-moon night wouldn’t have shined if not for a number of factors behind it. Less cloud-cover, less atmospheric pollutants, position of the moon itself but most importantly but overlooked is the sun that causes the reflection of the moon in the first place. To me, Future Academy has been the sun reflecting on all work I’m currently engaged with in the space of using evidence in doing the most good, the best way possible."    
                  text='Abdulrahman has a background in Economics, International Relations and Sustainable Coastal Management. He is now working with Ethical Seafood Research as a Sustainability and Blue Economy Policy Liaison Officer to Zanzibar. This involves pioneering Zanzibar’s Aquaculture Policy to ensure the industry is sustainable and ethical in respect to the human, environment and animal welfare.'
              />
            </div>
            <div className='col-md-4 col-sm-12'>
              <FellowCard
                  image='/fellows/Raphaelle-1.jpg'
                  cardName="Raphaëlle"
                  quote="Future Academy was a truly transformative experience that left a lasting impact on me. Thanks to its comprehensive curriculum, I realized just how pivotal this century could be in shaping the long-term future for better, or worse. It equipped me with the necessary tools and guidance to make a meaningful contribution. I am leaving Future Academy feeling better prepared and inspired to play my part in the making of this grand project."    
                  text='Raphaëlle has a background in classics, business economics and political science. Previously, she worked in a Foodtech startup accelerator and a climate tech company. She is part of the Global Shapers community and the Shaper’s Hub in Stockholm. She now works as an operations intern at Impact Academy. Future Academy inspired her to write her bachelor’s thesis on the Future of Life Institute’s security framing of autonomous weapons systems.'
              />
            </div>
            <div className='col-md-4 col-sm-12'>
              <FellowCard
                  image='/fellows/fellow_3.jpg'
                  cardName="Kolfinna"
                  quote="Future Academy has been an incredible place where I have had the opportunity to be surrounded by like- minded people who want to change the world. The cohort and faculty have inspired me to use my potential to the fullest, and I feel ready to embrace it."    
                  text='Kolfinna currently works as a Senior Adviser at the Icelandic Centre for Research. After finishing her studies in Law, International and the Settlement of Disputes and Middle- Eastern Studies, she wanted to make a larger impact on global issues. Alongside her job, she is working on various projects on nuclear risks in the current geopolitical climate. Kolfinna has published articles on the effectiveness and unintended consequences of economic sanctions as well as how activism is needed in the fight against sexual violence. Future Academy gave Kolfinna a new viewpoint on artificial intelligence, and her impact project was a project proposal on the implications artificial intelligence has on nuclear threats.'
              />
            </div>
            <div className='col-md-4 col-sm-12'>
              <FellowCard
                  image='/fellows/div.common-fellow-box_img4.png'
                  cardName="Maria"
                  quote="Future Academy came at the perfect moment in my life, during a gap year before university. It provided me with the opportunity to explore various ways to make a positive impact and discover impactful career paths I hadn’t previously considered. As I prepare to start university in October, I now feels equipped with the knowledge and inspiration to create a profound difference in the world."
                  text='Maria, 20 years old, based in Warsaw, Poland, has been an active climate activist and founder of several social projects for the past two years - e.g. the Fridays for Future Climate Education Team in Poland.'
              />
            </div>
            <div className='col-md-4 col-sm-12'>
              <FellowCard
                  image='/fellows/Georgii.jpg'
                  cardName="Georgii"
                  quote="Future Academy managed to help me greatly accelerate my personal development and career navigation and remove the anxiety that had been hindering me for a long time. These months have been transformative for my vision, goals, and well-being."
                  text='Georgii is 24 years old, based in Sweden and originally from Russia. He decided to make a transition from a corporate career and now utilizes his experience in management and operations to enhance food systems’ resilience to catastrophic risks. He also served an impact-oriented career navigation program for 300 high school students.'
              />
            </div>
            <div className='col-md-4 col-sm-12'>
              <FellowCard
                  image='/fellows/div.common-fellow-box_img6.png'
                  cardName="Heramb"
                  quote="Future Academy helped push me to broaden my horizons, connect with like-minded leaders, and develop valuable decision-making heuristics. This led to a successful AI Governance internship, and I recalibrated my career plans to do the most good."
                  text='Heramb, 21 years old, based in Roorkee, India, is a senior at IIT Roorkee and a mentor at the Clinton Global Initiative. He has run non-profits mapping over 2,000 critical aid locations and interned as a AI Governance Researcher at the Millenium Project.'
              />
            </div>
          </div>
          <div className='row'>
            <div className='col text-center pb-5'>
              <button className={`btn-rounded btn-blue btn-large w-fit-content fw-lighter`}>
                <span>More impactful journeys here</span> 
                <span className='ps-2'>
                  <Image
                    src="/arrow-right.png"
                    alt="arrow pointing right"
                    width={16}
                    height={16}
                  />
                </span> 
              </button>
            </div>
          </div>
        </section>
      </div>
    </main>
  )
}
